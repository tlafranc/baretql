"""
Reads from the file in.txt, where you can place any string that you want to analyze.
This offers a place where we can 'test-run' regexs before implementing them in tables.py
To change the text on which the testing is done, copy-paste text into 'in.txt'. This can be done
by first running './run.sh get' on the list page you want, then copy pasting the text you want to analyze
into 'in.txt'
"""

import sys
import re

def main():
    regexs = {
        r"&quot;": "\"", # HTML entities
        r"&amp;ndash;": "-",
        r"&amp;": "&",
        r"&lt;": "<",
        r"&gt;": ">",
        r"&ndash;": "-",
        r"br \/": " ",
        r"&nbsp;": " ", 
        r"colspan=\"?(\d+?)\"?.*?\|([^\|]*)\|?": lambda mO: ''.join((mO.group(2), '|')) * int(mO.group(1)),
        r"\[\[File:.*?(\]\])": "", # images / links
        r"(align|scope|rowspan|width)=(\")?.*?(\")?[^\|\[\{]*": "|",
        r"!": "|",
        r"{{abbr\|.*?\|(.*?)}}": r"\1", #Types of templates
        r"{{dts\|(.*?)}}": r"\1",
        r"{{DetailsLink\|.*?}}": "",
        r"{{ntsh\|.*?}}": "",
        r"{{sortname\|(.*?)\|(.*?)\|?.*?}}": r"\g<1> \g<2>",
        r"{{(.*?)}}?": r"\1",
        r"\[\[([^\|\]]*)\|?.*?\]\]": r"\1", # [[...]] links
        r";(\/)?\w*;": "", # HTML code (;small;, etc.)
        r"[\w-]*=\".*?\"":  "", # css styling
        r"'(?!s )": "",
        r"< >": " ",
        r"<(\\)?.*?(\/)?>": "", 
        r"[ ]*\|+[ ]*": "\", \"",
    }

    nestedRegexs = {
        r"\{\{(cite|efn)+[^\{\{]*?\}\}": "",
    }

    inFile = open('regexTesting/in.txt', 'r')
    outFile = open('regexTesting/out.txt', 'w')
    row = ''

    for line in inFile:
        line = line.strip()
        line = re.sub(r"^[^\|\[]", "|",line)
        row += line
    for regex in nestedRegexs:
        while re.search(regex, row):
            row = re.sub(regex, nestedRegexs[regex], row)
    for regex in regexs:
        row = re.sub(regex, regexs[regex], row)
        outFile.write("{:20}".format(regex) + row + '\n')

    # outFile.write(re.sub(regex, regexs[regex], row))

if __name__ == "__main__":
    main()