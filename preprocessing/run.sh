#!/bin/bash
# This is the central script for running the preprocessing files.

# 

if [ "$1" == "" ] || [ "$1" == "info" ]
then
    printf "Please enter one of the following commands:\n\n"
    printf "./run.sh regex:\n\tTest regular expressions using testing.py\n\n"
    printf "./run.sh get:\n\tGet specific list page from wiki dump, entered after running ./run.sh get\n\n"
    printf "./run.sh extract [-DEBUG] [NUM LINES]:\n\tExtract tables from list pages on latest local wiki dump\n"
    printf "\t[-DEBUG]: prints original lines and formatted lines to output file.\n"
    printf "\t[NUM LINES]: numerical value, reads this many lines * 1 million.\n"
    printf "\t\tTo read the entire file, type 'inf'\n"
    printf "\t\tDefaults to 1.\n"
    exit
fi
if [ "$1" == "regex" ]
then 
    tester=$(find -name testing.py)
    python $tester
else
    dump=$(find -name *.bz2 | sort | tail -1) # latest dump

    if [ "$1" == "extract" ]
    then 
        extractor=$(find -name extractTables.py)

        bunzip2 -c $dump | python $extractor $@
        
    elif [ "$1" == "get" ]
    then
        getter=$(find -name getListFiles.py)

        if [ "$2" == "" ]
        then
            echo "Please enter a list page to get:"
            echo -n "List of "
            read page
        else
            page=$2
        fi

        bunzip2 -c $dump | python $getter $page
    fi
fi