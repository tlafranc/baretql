import sys
import os
import re
import time
import math
import configparser
from R import R


def main():
    '''
    Main function that begins program.
    '''
    global user_inp_tables
    global relational_threshold
    global grouping_threshold
    global edit_dist_threshold
    global xc_operation
    global xc_multiplier

    # Extract global parameters. If set_vars returns false then end the
    # program.
    if not set_vars():
        return

    user_inp_tables = {}

    # Extract file from command line
    if (len(sys.argv) == 1):
        filename = "database.txt"
        print("USING %s AS FILE!" %(filename))
        #sys.exit()
    else:
        filename = sys.argv[1]

    # Parse file entered in command line. If parse_file returns false then
    # end the program.
    if parse_file(filename) == False:
        return

    # Welcome print statements
    print(
        "Default parameters are set in 'config.ini' and can be modified\n"
        + "using the 'set' command. Enter 'set' for more info on the set\n"
        + "command.")
    print('Type "help" if you need help.\n')
    print(">", end="", flush=True)

    # Determine whether input is through terminal or pipe
    if os.isatty(0):
        pipe = False
    else:
        pipe = True

    # Iterate over each line of user input
    for line in sys.stdin:
        line = line.strip("\n")
        lower = line.lower()

        if pipe:
            print(line)

        # Begin timer
        start_time = time.time()

        # If blank line, exit program
        if line == '':
            print(">", end="", flush=True)
            continue

        # Set command 
        elif lower[:4] == "set " or lower == "set":
            set_parameters(lower)

        # Help command
        elif lower == "help":
            print_help_message()

        # Print variables command
        elif lower == "params":
            print_parameters()

        # TODO Maybe change to 'elif line != ''', remove above 'if'? 
        else: 
            # Assignment operator in input
            if line.find("=") != -1:
                line = line.split("=")
                table_name = line[0].strip()
                dot_op = handle_dot_ops(line[1].strip())

                # dot_op returns false if there were no dot operation
                # succesfully performed.
                if not dot_op:
                    # If no dot operation, the user is potentially
                    # initializing a user inputed table
                    table_name, table = extract_table(line)
                    # table_name = None if improper table intialization
                    if table_name != None:
                        # Determine if all keys are not empty
                        bad_key = False
                        for row in table:
                            key = row[0]
                            if key == '':
                                print("Table cannot contain a row with"
                                      + "an empty key.")
                                bad_key = True
                                break
                        if not bad_key:
                            user_inp_tables[table_name] = R(table)
                            print(user_inp_tables[table_name])
                # Assign the table to the result of the dot operation if 
                # a table was returned
                elif type(dot_op) != bool:
                    user_inp_tables[table_name] = dot_op
            else:
                dot_op = handle_dot_ops(line)
                # If no dot operation was performed then the user input
                # was invalid
                if not dot_op:
                    print("Unknown command or unknown table.")   
        
        # If verbose parameter set to true, display time it took for operation
        if verbose == "on":
            tot_time = time.time() - start_time
            print("Operation Time: %.3f" %tot_time)
        # End of operation print statements
        print("------------------------------\n")
        print(">", end="", flush=True)

def set_vars():
    '''
    Function that sets the global variables of the python file by reading the
    config.ini file.

    Returns:
        True if succesfully extracted all parameters, false otherwise.
    '''
    global relational_threshold
    global grouping_threshold
    global edit_dist_threshold
    global xc_operation
    global xc_multiplier
    global xc_empty_threshold
    global fill_operation
    global fill_multiplier
    global verbose

    config = configparser.ConfigParser()
    config.read('config.ini')
    # Make sure that numerical parameters are indeed numerical
    try:
        relational_threshold = float(config['parameters']['related_threshold'])
        grouping_threshold = float(config['parameters']['grouping_threshold'])
        edit_dist_threshold = float(config['parameters']['editdist_threshold'])
        xc_multiplier = float(config['parameters']['xc_smoothing_multiplier'])
        xc_empty_threshold = float(config['parameters']['xc_empty_threshold'])
        fill_multiplier = float(config['parameters']['fill_smoothing_multiplier'])
    except ValueError:
        # Report that not all numerical parameters are numerical
        print('ERROR')
        print('Related_threshold, grouping_threshold, editdist_threshold,\n'
              + 'xc_smoothing_multiplier, the xc_empty_threshold, and the\n'
              + 'fill_smoothing_multiplier must all be floating point\n'
              + 'numbers in config.ini.')
        return False

    # Make sure the operation for xc is 'sum' or 'product'
    xc_operation = config['parameters']['xc_group_scoring']
    xc_operations = ["sum", "product"]
    if xc_operation not in xc_operations:
        print('ERROR')
        print('xc_group_scoring must be set to either "sum" or "product".')
        return False

    # Make sure the operation for fill is 'sum', 'product' or 'probabilistic'
    fill_operation = config['parameters']['fill_group_scoring']
    fill_operations = ["sum", "product", "probabilistic"]
    if fill_operation not in fill_operations:
        print('ERROR')
        print('fill_group_scoring must be set to either "sum", "product" or\n'
              + '"probabilistic".')
        return False
    
    # Make sure verbose parameter is either 'on' or 'off'
    verbose = config['parameters']['verbose'].lower()
    options = ["on", "off"]
    if verbose not in options:  
        print("ERROR")
        print('verbose parameter must be set to on or off.')
        return False

    return True

def parse_file(filename):
    ''' 
    Function used to extract all tables from a text file.

    Arguments:
        filename: The name of the file to parse the tables from.

    Returns:
        True if the file was succesfully parsed, false otherwise.
    '''
    global num_tables

    tables_list = []
    key_table_index = {}

    # Variable that contains the different percentages where when parsing
    # at each percentage, let the user know *percentage* number of 
    # tables have been parsed.
    thresholds = [10, 25, 50, 75, 90, 100]

    # https://stackoverflow.com/a/6475407
    # accessed on 2018-07-04
    try:
        with open(filename, "r", encoding="UTF-8") as infile:
            print("\nParsing file '%s'." %(filename))
            print("This may take a while if the file is large.")
            # Variable header is set to True one the program has already
            # tried to parse the header line and False otherwise.
            # Variable table_count is set to True if the table count was
            # succesfully retrieved from the header line and False otherwise.
            table = '' 
            header = False
            table_count = False
            i = 0
            for line in infile:
                if not header:
                    # Determine whether first line of file was a header line
                    # containing number of tables, average row length and
                    # average column length
                    header = True
                    line = line.strip('\n').split()
                    try:
                        num_tables = int(line[0])
                        table_count = True
                        avg_row_len = float(line[1])
                        avg_col_len = float(line[2])
                    except (ValueError, IndexError):
                        if table_count == False:
                            print("\nNo table count description at the"
                                  + " top of the text file.")
                    continue
                
                # Empty line can indicate end of a table
                if line == '\n':
                    # If table is empty, go to next line in file.
                    if table == '':
                        continue
                    # If table is not empty, extract each row and 
                    # add each key to key_table_index.
                    table = table.strip('\n')
                    rows = []
                    for entry in table.split("\n"):
                        entry = extract_entries(entry.split(","))
                        key = entry[0].lower()
                        if key in key_table_index:
                            key_table_index[key].append(i)
                        else:
                            key_table_index[key] = [i]
                        rows.append(tuple(entry))
                    # Create table and append it to table list
                    tables_list.append(R(rows))
                    i += 1
                    # Print statements to update user on how much
                    # of the file has been parsed.
                    if table_count:
                        score = math.floor((i / num_tables) * 100)
                        if score in thresholds:
                            print("%d%% of the tables parsed..." %score)
                            thresholds.remove(score)
                    else:
                        if (i % 100000 == 0) and i != 0:
                            print("%d tables parsed..." %i)
                    # Clear the table string
                    table = ''                   
                else:
                    table += line
    except FileNotFoundError:
        print("\nFile '%s' does not exist." %filename)
        return False

    print("\nParsing complete.")

    # Set the class variables tables_list and key_table_index of R
    R.table_list = tables_list
    R.key_table_index = key_table_index
    
    num_tables = len(tables_list)
    return True

def extract_entries(entries):
    '''
    Function used in order to extract row entries from an inputed row.

    Arguments:
        entries: A row input that was split upon ","
    
    Returns:
        A tuple of the inputed row without quotations marks
        and where each entry is separated by a ","
    '''
    # new_entry is a list of the entries where each element is an
    # entry for one column.
    new_entry = []
    i = 0
    while i < len(entries):
        entries[i] = entries[i].strip(" ")
        # If empty string, append it to new_entry
        if len(entries[i]) == 0:
            new_entry.append(entries[i])
        # If entry begins with ", determine if it also ends with ".
        # If it does not, find the rest of the row entry for this column.
        # Otherwise, append it to new_entry without the quotation marks.
        elif entries[i][0] == '"':
            if entries[i][-1] != '"':
                i = find_corresponding_pair(entries, new_entry, i, '"')
            else:
                new_entry.append(entries[i][1:-1])
        # If entry begins with ', determine if it also ends with '.
        # If it does not, find the rest of the row entry for this column.
        # Otherwise, append it to new_entry without the quotation marks.
        elif entries[i][0] == "'":
            if entries[i][-1] != "'":
                i = find_corresponding_pair(entries, new_entry, i, "'")
            else:
                new_entry.append(entries[i][1:-1])            
        else:
            new_entry.append(entries[i].strip(' "\''))
        i += 1
    return tuple(new_entry)

def find_corresponding_pair(entries, new_entry, ind, separator):
    '''
    Function used in order help with splitting a string on commas
    only if the comma is not between quotation marks or apostrophes.
    This function concatenates the split up portion of a string if it 
    had quotation marks or apostrophes and a comma.

    Arguments:
        entries: A list that was a string split on commas
        new_entry: The prior entries for the row
        ind: The beginning index of the string that has been split up
        separator: Indicates if it was either an apostrophe or a
            quotation mark used to encapsulate the entry

    Returns:
        The index from where to continue the parsing 
        of the user inputed entries
    '''
    entry = entries[ind]
    if entries[ind][-1] != separator:
        # Iterate over each other entry and find the one that ends with
        # a quotation mark. Once found, end the for loop.
        for j in range(ind + 1, len(entries)):
            entry += "," + entries[j]
            try:
                if entries[j][-1] == separator:
                    break
            except IndexError:
                continue
    # Append the concatenated entry without the quotation marks or apostrophes
    new_entry.append(entry.strip(separator))
    try:
        return j
    except NameError:
        return ind

def set_parameters(line):
    '''
    Function that sets the parameters of BareTQL.

    Arguments:
        line: Command input
    '''
    global relational_threshold
    global grouping_threshold
    global edit_dist_threshold
    global xc_operation
    global xc_multiplier
    global xc_empty_threshold
    global fill_operation
    global fill_multiplier
    global verbose

    xc_operations = ["sum", "product"]
    fill_operations = ["sum", "product", "probabilistic"]
    verbose_options = ["on", "off"]
    line = line.split()
    length = len(line)
    # If the line was only 'set' then display a help message on how to
    # use the set command
    if length == 1 and line[0] == "set":
        print(
            "You can set the global parameters of the program by entering:\n"
            + "set related_threshold *value*\n"
            + "set grouping_threshold *value*\n"
            + "set editdist_threshold *value*\n"
            + "set group_scoring *xc operations*\n"
            + "set xc_multiplier *value*\n"
            + "set xc_empty_threshold *value*\n"
            + "set fill_operation *fill operations*\n"
            + "set fill_multiplier *value*\n"
            + "set verbose *verbose options*\n\n"
            + "where 0 <= value <= 1, xc operations are sum or product,\n"
            + "fill operations are sum, product or probabilistic and\n"
            + "verbose options are on or off\n")
        print_parameters()

    # If there were 3 strings in the command separated by spaces
    elif length == 3:
        # Determine if the last string is a floating number
        try:
            line[2] = float(line[2])
            # If the last string was a floating number, determine
            # if it was for the related_threshold, grouping_threshold
            # or editdist_threshold parameters and if the value
            # was between 0 and 1.
            if line[2] < 0 or line[2] > 1:
                print("Value chosen for set command is not between 0 and 1")
            elif line[1] == "related_threshold":
                relational_threshold = line[2]
                print("Related threshold set to %.3f" % (relational_threshold))
            elif line[1] == "grouping_threshold":
                grouping_threshold = line[2]
                print("Grouping threshold set to %.3f" % (grouping_threshold))
            elif line[1] == "editdist_threshold":
                edit_dist_threshold = line[2]
                print("Edit distance threshold set to %.3f" 
                      % (edit_dist_threshold))
            elif line[1] == "xc_multiplier":
                xc_multiplier = line[2]
                print("xc multiplier set to %.3f" % (xc_multiplier))
            elif line[1] == "xc_empty_threshold":
                xc_empty_threshold = line[2]
                print("xc empty threshold set to %.3f" % (xc_empty_threshold))
            elif line[1] == "fill_multiplier":
                fill_multiplier = line[2]
                print("fill multiplier set to %.3f" % (fill_multiplier))
            else:
                print("Improper formatting for set command")
        except ValueError:
            # If the last string was not a floating number, determine
            # if it was for the group_scoring parameter and if 
            # the last string was either sum or product.
            if line[1] == "group_scoring" and line[2] in xc_operations:
                xc_operation = line[2]
                print("Operation set to %s" % (xc_operation))
            elif line[1] == "fill_operation" and line[2] in fill_operations:
                fill_operation = line[2]
                print("Operation set to %s" % (fill_operation))
            elif line[1] == "verbose" and line[2] in verbose_options:
                verbose = line[2]
                print("Verbose parameter set to %s" % (verbose))
            else:
                print("3rd argument was not a float.")
    else:
        print("Improper set command")

def handle_dot_ops(line):
    '''
    Function used to handle the dot operations of this program 
    (.related, .related.i, .xr, .xc, .fill, .insert, 
    .delete, .delrow, .delcol, .delcell, .updatecell, 
    .sort, .swap).

    Arguments:
        line: The user inputed line in the form of 
            *tablename*.*dotOperation*

    Returns:
        A boolean or table. Return false if the input was not a dot op.
        Return True if there is nothing to print (most likely due to
        an incomplete dot operation) and a table otherwise.
    '''
    global user_inp_tables
    global relational_threshold
    global grouping_threshold
    global edit_dist_threshold
    global xc_operation
    global xc_multiplier
    global xc_empty_threshold
    global fill_operation
    global fill_multiplier
    global num_tables

    multi_allowed_ops = ["related", "xr", "xc", "fill"]
    non_multi_ops = [
        "insert", "delete", "delrow", 
        "delcol", "sort", "swap", 
        "delcell", "updatecell"
    ]
    line = line.split(".")
    table_name = line[0] 
    # If table_name not initialized already then no dot operation can
    # be performed
    if table_name not in user_inp_tables:
        return False
    else:
        table = user_inp_tables[table_name]

    ops = line[1:]
    num_ops = len(ops)
    # If there are more than one operation, make sure they are all multiple
    # allowed operations. If not return false. (In the case of related.i, 
    # since we are separating upon .'s, make sure the operation before was
    # 'related')
    if num_ops > 1:
        for i in range(num_ops):
            if ops[i].lower() not in multi_allowed_ops:
                if not (is_integer(ops[i]) and i > 0 and ops[i - 1] == "related"):
                    return False
    # If there is only one operation make sure it is valid operation
    elif num_ops == 1:
        op = ops[0].split("(")[0].lower()
        if (op not in non_multi_ops) and (op not in multi_allowed_ops):
            return False
    # num_ops == 0
    # Print table command or setting a new table to an already existing table.
    else:
        print(table)
        return R(table.get_rows())

    for op in ops:
        relatedi = False
        related = False
        lowercase_op = op.lower()
        if lowercase_op == "related":
            # Operation is related, call .related on the table
            related = True
            tables = table.related(relational_threshold, edit_dist_threshold)

        elif is_integer(op):
            # Operation is an integer, call related.i on the table
            index = int(op)
            table, value = table.get_related_by_index(index)
            relatedi = True
            
        elif lowercase_op == "xr":
            # Case .xr command
            table = table.xr(relational_threshold, 
                             edit_dist_threshold, num_tables)

        elif lowercase_op == "xc":
            # Case .xc command
            table = table.xc(relational_threshold, grouping_threshold, 
                             xc_empty_threshold, xc_operation, 
                             xc_multiplier, edit_dist_threshold)

        elif lowercase_op == "fill":
            # Case .fill command
            table = table.fill(relational_threshold, fill_operation, 
                               fill_multiplier, edit_dist_threshold)

        elif lowercase_op[:6] == "insert":
            # .insert command
            match_obj = re.match("insert\((.*?)\)$", lowercase_op)
            if match_obj:
                # Need this new re.search to not lowercase user input
                match_obj = re.search("\((.*?)\)$", op)
                row = match_obj.group(1)
                row = extract_entries(row.split(","))
                if not table.insert(row):
                    return True
            else:
                print("Improper .insert command.")
                return True
        
        elif lowercase_op[:6] == "delete":
            # .delete command
            match_obj = re.match("delete\((.*?)\)$", lowercase_op)
            if match_obj:
                # Need this new re.search to not lowercase user input
                match_obj = re.search("\((.*?)\)$", op)
                row = match_obj.group(1)
                row = extract_entries(row.split(","))
                if not user_inp_tables[table_name].delete(row):
                    return True
            else:
                print("Improper .delete command.")
                return True

        elif lowercase_op[:6] == "delrow":
            # delrow command
            # Determine whether it is a single delete (match_obj1) or
            # if it is a range delete (match_obj2)
            match_obj1 = re.match("delrow\((\d+)\)$", lowercase_op)
            match_obj2 = re.match("delrow\((\d+)-(\d+)\)$", lowercase_op)
            if match_obj1:
                ind = int(match_obj1.group(1))
                if not user_inp_tables[table_name].del_row(ind):
                    return True
            elif match_obj2:
                beg_ind = int(match_obj2.group(1))
                end_ind = int(match_obj2.group(2))
                if beg_ind > end_ind:
                    print("First index must be smaller than second.")
                    return True
                elif beg_ind >= 1:
                    for ind in range(end_ind, beg_ind - 1, -1):
                        if not user_inp_tables[table_name].del_row(ind):
                            return True
                else:
                    print("First index is too small.")
                    return True
            else:
                print("Improper .delRow command.")
                return True

        elif lowercase_op[:6] == "delcol":
            # delcol command
            # Determine whether it is a single delete (match_obj1) or
            # if it is a range delete (match_obj2)
            match_obj1 = re.match("delcol\((\d)\)$", lowercase_op)
            match_obj2 = re.match("delcol\((\d+)-(\d+)\)$", lowercase_op)
            if match_obj1:
                ind = int(match_obj1.group(1))
                if not user_inp_tables[table_name].del_col(ind): 
                    return True
            elif match_obj2:
                beg_ind = int(match_obj2.group(1))
                end_ind = int(match_obj2.group(2))
                if beg_ind > end_ind:
                    print("First index must be smaller than second.")
                    return True
                elif beg_ind >= 1:
                    for ind in range(end_ind, beg_ind - 1, -1):
                        if not user_inp_tables[table_name].del_col(ind):
                            return True
                else:
                    print("First index is too small.")
                    return True
            else:
                print("Improper .delCol command.") 
                return True 

        elif lowercase_op[:7] == "delcell":
            # .delcell command
            match_obj = re.match("delcell\((\d+),[ ]*(\d+)\)$", lowercase_op) 
            if match_obj:
                row_index = int(match_obj.group(1))
                col_index = int(match_obj.group(2))
                if not table.delcell(row_index, col_index):
                    return True
            else:
                print("Improper .delcell command")
                return True

        elif lowercase_op[:10] == "updatecell":
            # .updatecell command
            match_obj = re.match("updatecell\((\d+),[ ]*(\d+),[ ]?([^ \)]+)\)$", 
                                 lowercase_op) 
            if match_obj:
                # Need this new match in order to not lowercase entry
                match_obj = re.search("\((\d+),[ ]*(\d+),[ ]?([^ \)]+)\)$", op)
                row_index = int(match_obj.group(1))
                col_index = int(match_obj.group(2))
                entry = match_obj.group(3)
                if entry == "''" or entry == '""':
                    entry = ''
                if not table.update_cell(row_index, col_index, entry):
                    return True
            else:
                print("Improper .updateCell command")
                return True
        
        elif lowercase_op[:4] == "sort":
            # .sort command
            match_obj = re.match("sort\((.*?)\)$", lowercase_op)
            if match_obj:
                args = match_obj.group(1)
                args = args.split(",")
                if len(args) == 1:
                    # len(args) == 1 so the only parameter is the column index
                    # to sort upon
                    if not is_integer(args[0]):
                        # If column index was not an integer than print out
                        # help message
                        print(
                            "sort the table on any column by entering '*tablename*.sort(i)'\n"
                            + "or '*tablename*.sort(i, bool)' where i is an integer between 1\n"
                            + "and the number of columns in *tablename* and bool is an optional\n"
                            + "boolean indicating if the table should be sorted in descending\n"
                            + "order. Table is sorted in ascending order by default")
                        return True           
                    # If only parameter is integer then call .sort on table    
                    if not table.sort(int(args[0]), False):
                        return True

                elif len(args) == 2:
                    # len(args) == 2 so the user provided both a column index 
                    # to sort upon and if they want it in ascending order
                    if not is_integer(args[0]):
                        # If column index was not an integer than print out
                        # help message
                        print(
                            "sort the table on any column by entering '*tablename*.sort(i)'\n"
                            +  "or '*tablename*.sort(i, bool)' where i is an integer between 1\n"
                            +  "and the number of columns in *tablename* and bool is an optional\n"
                            +  "boolean indicating if the table should be sorted in descending\n"
                            +  "order. Table is sorted in ascending order by default")
                        return True  
                    # Determine if second parameter was true or false
                    desc_param = args[1].strip()
                    if desc_param == "true":
                        if not table.sort(int(args[0]), True):
                            return True
                    elif desc_param == "false":
                        if not table.sort(int(args[0]), False):
                            return True   
                    else:
                        print("desc parameter must be true or false")
                        return True
                else:
                    print("Too many arguments")
                    return True
            else:
                print("Improper .sort command.") 
                return True

        elif lowercase_op[:4] == "swap":
            # .swap command
            match_obj = re.match("swap\((\d+),[ ]*(\d+)\)$", lowercase_op)
            if match_obj:
                indx1 = int(match_obj.group(1))
                indx2 = int(match_obj.group(2))
                if not table.swap(indx1, indx2):
                    return True
            else:
                print("Improper .swap command")
                return True
                       
        else:
            return False 

    if related:
        # Related command so print the rows of each table
        for table in tables:
            print(table.get_rows())
        print("# of related tables: %d" %len(tables))
    elif relatedi:
        # Related i command so print out number of overlap related
        # table i had with the query table
        if table != None:
            print(table)
            print("\nOverlap = %.3f" %(value))
        else:
            return True
    else:
        # For any other command, print the table
        print(table) 
    return table

def extract_table(line_segments):
    '''
    Function used in order to extract the user inputed relational table.

    Arguments:
        line_segments: A list of the line segments where the first element 
            references the table name and the second element references the 
            table elements

    Returns:
        The table name and relational table extracted from user input 
        or None, None if input was invalid.
    '''
    table_name = line_segments[0].strip(" ")
    table = []
    line_segments[1] = line_segments[1].strip(" ")
    # INPUT Method 1. Multiple rows separated by ),(
    if (line_segments[1].find('),(') != -1 and 
        line_segments[1].find("((") != -1 and 
        line_segments[1].find("))") != -1):
        # [2:-2] is to remove '((' and '))'
        for entries in line_segments[1][2:-2].split('),('):
            entries = extract_entries(entries.split(","))
            table.append(entries)
    # INPUT Method 2. Multiple rows separated by ), (
    elif (line_segments[1].find('), (') != -1 and
          line_segments[1].find("((") != -1 and 
          line_segments[1].find("))") != -1): 
        # [2:-2] is to remove '((' and '))'
        for entries in line_segments[1][2:-2].split('), ('):
            entries = extract_entries(entries.split(","))
            table.append(entries)
    # INPUT Method 3. Singular row with '((' and '))' as beginning and ending tags
    elif (line_segments[1].find("((") != -1 and 
          line_segments[1].find("))") != -1):
        # [2:-2] is to remove '((' and '))'
        entries = extract_entries(line_segments[1][2:-2].split(","))
        table.append(entries)
    # Input Method 4. Singular row with '(' and ')' as beginning and ending tags
    elif (line_segments[1].find("(") != -1 and 
          line_segments[1].find(")") != -1 and 
          line_segments[1].find("((") == -1):
        # [1:-1] is to remove '((' and '))'
        entries = extract_entries(line_segments[1][1:-1].split(","))
        table.append(entries)
    # Improper input
    else:
        print(
            "Improper entry.\nEnter a table in the form R = "
            + "((i11, i12),(i21, i22)) or R = ((i11, i12), (i21, i22)).")
        return None, None
    return table_name, table

def print_parameters():
    '''
    Function that prints out the parameters
    '''
    global relational_threshold
    global grouping_threshold
    global edit_dist_threshold
    global xc_operation
    global xc_multiplier
    global xc_empty_threshold
    global fill_operation
    global fill_multiplier
    global verbose

    print("Currently:\n"
          + "related_threshold is %.2f\n" % (relational_threshold)
          + "grouping_threshold is %.2f\n" % (grouping_threshold)
          + "editdist_threshold is %.2f\n" % (edit_dist_threshold)
          + "group_scoring is %s\n" % (xc_operation)
          + "xc_multiplier is %.2f\n" % (xc_multiplier)
          + "xc_empty_threshold is %.2f\n" % (xc_empty_threshold)
          + "fill_operation is %s\n" % (fill_operation)
          + "fill_multiplier is %.2f\n" % (fill_multiplier)
          + "verbose is %s\n" % (verbose))

def print_help_message():
    '''
    Function that displays a help message.
    '''
    print(
        "To initialize a relational table, type \n"
        + "'*table name* = ((*r1c1*, *r1c2*, ..., *r1cn*), "
        + "(*r2c1*, *r2c2*, ..., *r2cn*), ..., "
        + "(*rnc1*, *rnc2*, ..., *rncn*))'\n"
        + "e.g. R = ((John, 21), (Mary, 22), (Jeff, 20))\n"
        + "If you need to write a comma in a row entry, "
        + "encapsulate the row entry in\n"
        + "either quotation marks or apostrophes e.g.\n"
        + "R = ((\"Doe, John\", 21), ('Jane, Mary', 22), (Jeff, 20))\n\n"
        + "After creating a table you can:\n"
        + "    - find related tables by typing by entering '*tablename*.related'\n"
        + "    - find a particular related table by doing '*tablename*.related.i'\n"
        + "      where i is index between 1 and the number of related tables\n"
        + "    - extend the rows of a table by entering '*tablename*.xr'\n"
        + "    - extend the columns of a table by entering '*tablename*.xc'\n"
        + "    - sort the table on any column by entering '*tablename*.sort(i)'\n"
        + "      or '*tablename*.sort(i, bool)' where i is an integer between 1\n"
        + "      and the number of columns in *tablename* and bool is an optional\n"
        + "      boolean indicating if the table should be sorted in descending\n"
        + "      order. Table is sorted in ascending order by default\n"
        + "    - Swap two columns in a table by entering '*tablename*'.swap(i, j)\n"
        + "      where i and j are integers between 1 and the number of columns\n"
        + "      in *tablename*\n"
        + "    - insert a row into the table by entering\n"
        + "      '*tablename*.insert(*r1c1*, *r1c2*, ..., *r1cn*)'\n"
        + "    - delete a row from the table by entering\n"
        + "      '*tablename*.insert(*r1c1*, *r1c2*, ..., *r1cn*)' provided\n"
        + "      this row is already in *tablename*\n"
        + "    - delete a row from the table by entering\n"
        + "      '*tablename*.delrow(index)' where index is an integer between\n"
        + "      1 and the number of rows in *tablename*\n"
        + "    - delete a column from the table by entering\n"
        + "      '*tablename*.delcol(index)' where index is an integer between\n"
        + "      1 and the number of columns in *tablename*\n\n"
        + "You are able to combine multiple related, related.i, xr and xc\n"
        + "operations into one. e.g. R.related.2.xr.xc\n\n"
        + "Enter 'set' for information on setting the parameters of the\n"
        + "program. Enter 'params' to find out what the value of each\n"
        + "parameter is currently.")

def is_integer(inp):
    '''
    Function used to determine if the input was an integer.
    
    Arguments:
        inp: Input
        
    Returns:
        A boolean indicating if the input was an integer or not
    '''
    try:
        int(inp)
        return True
    except ValueError:
        return False

main()
