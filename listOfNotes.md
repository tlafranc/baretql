# Description
This page contains what we have noticed from getListFiles.py about wikipedia's
'List of ...' files. We want to determine what separates a page that contains a table
from a page that contains a list of links to other pages, since we want to extract the tables
and ignore the lists. 


## Observations about data
Contains observations about the data structure, not necessarily the different tables

- '[[...]]' defines a link
- '


## Table data

- There are a few different ways to define a table. They are described below.


#### 'wikitable' table
- Defined by the first line "{| class=&quot;wikitable sortable&quot;"
- Before the data, columns are defined with a '!' at the start of the line
- 'data-sort-value=...' defines a sorting parameter, not data itself
- TENTATIVE: columns formatting is separated from data with a ';|' string.

#### 'Bracket notation: present on the page 'List of multiplanetary systems'
- Begins with a line of the form "{{ [page title] list/Top}}
- Each table row is defined as follow:
    - {{ [page title] list
        | 'field' = 'value'
        | 'field' = 'value'
        ...
        | 'field' = 'value'
    }}

- The table ends with a line of the form '{{ [page title] list/Bottom}}

### dynamic list
- Defined in much the same way as 'wikitable', but starts with '{{dynamic list}};
- 

## Link data

- Each line is defined as '** [[page link]]
- Since it is not defined to be a wikitable, no special syntax is present for defining rows, columns, or anything else that is characteristic of a 'table'.

## indents data

- Similar to the link data, except each indentation from the previous row
is defined by the addition of single quotes (') before the link syntax.